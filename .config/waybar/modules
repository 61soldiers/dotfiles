{
  "sway/workspaces": {
    "active-only": false,
    "all-outputs": true,
    "disable-scroll": true,
    "format": "{icon}",
    "show-special": false,
    "on-click": "activate"
  },
  "clock": {
    "interval": 1,
    "format": "  {:%H:%M} ",
    "format-alt": "  {:%H:%M  |  %d %B %Y, %A} ",
    "tooltip-format": "<tt><small>{calendar}</small></tt>",
    "calendar": {
      "mode": "month",
      "mode-mon-col": 3,
      "weeks-pos": "right",
      "on-scroll": 1,
      "format": {
        "days": "<span color='#ebdbb2'><b>{}</b></span>",
        "weeks": "<span color='#99ffdd'><b>W{}</b></span>",
        "weekdays": "<span color='#ebdbb2'><b>{}</b></span>",
        "today": "<span color='#ff6699'><b><u>{}</u></b></span>"
      }
    }
  },
  "actions": {
    "on-click-right": "mode",
    "on-click-forward": "tz_up",
    "on-click-backward": "tz_down",
    "on-scroll-up": "shift_up",
    "on-scroll-down": "shift_down"
  },
  "cpu": {
    "format": " CPU: {usage}% ",
    "interval": 1,
    "on-click-right": "gnome-system-monitor"
  },
  "memory": {
    "interval": 10,
    "format": " {percentage}% ",
    "format-alt-click": "click",
    "tooltip": true,
    "tooltip-format": "{used:0.1f}GB/{total:0.1f}G",
    "on-click-right": "kitty -c ~/.config/dotfiles/kitty/kitty.conf --title btop sh -c 'btop'"
  },
  "mpris": {
    "interval": 10,
    "format": " {title} ",
    "format-paused": " {title} {status_icon} ",
    "on-click": "playerctl play-pause",
    "on-click-right": "playerctl next",
    "scroll-step": 5.0,
    "smooth-scrolling-threshold": 1,
    "status-icons": {
      "paused": "󰐎",
      "playing": "🎶",
      "stopped": ""
    },
    "max-length": 30
  },
  "pulseaudio": {
    "format": " {volume}% {icon} ",
    "format-bluetooth": " 󰂰 {volume}% {icon} ",
    "format-muted": "🔇",
    "format-icons": {
      "headphone": "🔈",
      "hands-free": "🔈",
      "headset": "🔈",
      "phone": "🔈",
      "portable": "🔈",
      "car": "🔈",
      "default": [
        "🔈",
        "🔈",
        "🔈",
        "🔈"
      ],
      "ignored-sinks": [
        "Easy Effects Sink"
      ]
    },
    "scroll-step": 5.0,
    "tooltip-format": "{icon} {desc} | {volume}%",
    "smooth-scrolling-threshold": 1
  },
  "wlr/taskbar": {
    "format": " {icon} ",
    "icon-size": 20,
    "all-outputs": false,
    "tooltip-format": "{title}",
    "on-click": "activate",
    "on-click-middle": "close",
    "ignore-list": [
      "rofi",
      "firefox",
      "kitty",
      "jetbrains-studio",
      "Brave-browser",
      "Spotify"
    ]
  },
  "custom/separator": {
    "format": "|",
    "interval": "once",
    "tooltip": false
  }
}
