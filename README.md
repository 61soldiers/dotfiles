# LALALAND (arch)

## Display Manager

[SDDM Astronaut theme](https://github.com/Keyitdev/sddm-astronaut-theme/tree/master)

## GTK Theme

[ZorinPurple-Dark](https://github.com/ZorinOS/zorin-desktop-themes)

## Rofi

[adi1090x theme collection](https://github.com/adi1090x/rofi) - Type 6

## Required fonts

[Libre Baskerville](https://fonts.google.com/specimen/Libre+Baskerville)
